version: '3.4'

networks:
  chainpoint:
    driver: bridge

services:
  # Tendermint and ABCI app in go. Coordinates the calendar
  abci:
    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/go-abci-service:${DOCKER_TAG}
    build:
      context: .
      dockerfile: docker/Dockerfile.abci
    depends_on:
      - rabbitmq
      - api
    user: ${USERID}:${GROUPID}
    environment:
      PEERS: ${PEERS}
      CHAINPOINT_CORE_BASE_URI: ${CHAINPOINT_CORE_BASE_URI}
      TENDERMINT_HOST: 127.0.0.1
      TENDERMINT_PORT: 26657
      NETWORK: ${NETWORK:-testnet}
      AUDIT: 'true'
      NODE_MANAGEMENT: 'true'
      ANCHOR: 'true'
      AGGREGATE: 'true'
      AGGREGATION_THREADS: '4'
      HASHES_PER_MERKLE_TREE: '25000'
      ANCHOR_INTERVAL: '3'
      LOG_FILTER: 'main:debug,state:info,*:error'
      LOG_LEVEL: 'info'
      ANCHOR_TIMEOUT: '10'
      LN_STAKE: '10000'
    volumes:
      - ~/.chainpoint/core/config/node_1:/tendermint/config:Z
      - ~/.chainpoint/core/config/node_1/data:/tendermint/data:Z
      - ~/.chainpoint/core/data/keys/ecdsa_key.pem:/run/secrets/ECDSA_PKPEM
      - ~/.chainpoint/core/.lnd:/root/.lnd:z
    ports:
      - '26656:26656'
      - '26657:26657'
    command: bash -c "abci-service"
    networks:
      - chainpoint

  # Lightning node
  lnd:
    image: tierion/lnd:${NETWORK:-testnet}-0.9.2
    user: ${USERID}:${GROUPID}
    restart: always
    entrypoint: './start-lnd.sh'
    ports:
      - target: 8080
        published: 8080
        protocol: tcp
        mode: host
      - target: 9735
        published: 9735
        protocol: tcp
        mode: host
      - target: 10009
        published: 10009
        protocol: tcp
        mode: host
    environment:
      - PUBLICIP=${CORE_PUBLIC_IP_ADDRESS}
      - HOT_WALLET_ADDRESS=${HOT_WALLET_ADDRESS}
      - RPCUSER
      - RPCPASS
      - NETWORK=${NETWORK:-testnet}
      - CHAIN
      - DEBUG
      - BACKEND=neutrino
      - NEUTRINO
      - LND_REST_PORT
      - LND_RPC_PORT
      - TLSPATH
      - TLSEXTRADOMAIN=lnd
    volumes:
      - ~/.chainpoint/core/.lnd:/root/.lnd:z
    networks:
      - chainpoint

  # Node.js Base Image
  # See : https://stackoverflow.com/questions/37933204/building-common-dependencies-with-docker-compose
  #
  base:
    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/node-base:${DOCKER_TAG}
    build:
      context: .
      dockerfile: docker/Dockerfile.node-base

  # HTTP API
  # Restify Node public web API service.
  #
  # Note : You don't access this directly, but through
  # the nginx-proxy load balancer. This service cannot be
  # given an explicit container_name since it needs
  # to be scalable with 'docker-compose scale api=5'
  #
  # The VIRTUAL_HOST env var is used by the nginx-proxy
  # to rebuild its reverse proxy host config and must be
  # passed in by the HTTP client:
  #
  # Start With:
  # docker-compose up -d --build nginx-proxy
  #
  # curl -i http://127.0.0.1/
  #
  # PORTS : 8080
  #
  api:
    extra_hosts:
      - 'outside:127.0.0.1'
    restart: always
    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/node-api-service:${DOCKER_TAG}
    build:
      context: .
      dockerfile: docker/Dockerfile.api
    depends_on:
      - base
      - redis
      - rabbitmq
      - postgres
      - lnd
    user: ${USERID}:${GROUPID}
    networks:
      - chainpoint
    ports:
      - target: 8080
        published: 80
        protocol: tcp
        mode: host
      - target: 8443
        published: 443
        protocol: tcp
        mode: host
    environment:
      AGGREGATOR_WHITELIST: 127.0.0.1
      AGGREGATOR_PUBLIC: 'false'
      NODE_ENV: ${NODE_ENV}
      SUBMIT_HASH_PRICE_SAT: ${SUBMIT_HASH_PRICE_SAT:-2}
      NETWORK: ${NETWORK:-testnet}
      CHAINPOINT_CORE_BASE_URI: ${CHAINPOINT_CORE_BASE_URI}
      LND_SOCKET: ${LND_SOCKET}
      HOT_WALLET_ADDRESS: ${HOT_WALLET_ADDRESS}
      VIRTUAL_HOST: api.local
      TENDERMINT_URI: http://abci:26657
      UV_THREADPOOL_SIZE: 128
      LND_MACAROON: ${LND_MACAROON}
      LND_TLS_CERT: ${LND_TLS_CERT}
      SESSION_SECRET: ${SESSION_SECRET}
    volumes:
      - ~/.chainpoint/core/.lnd:/root/.lnd
      - ~/.chainpoint/core/data/keys/ecdsa_key.pem:/run/secrets/ECDSA_PKPEM
      - ./node-api-service/lib/endpoints:/home/node/app/lib/endpoints/
      - ./node-api-service/server.js:/home/node/app/server.js
    tty: true

  api-test:
    container_name: node-api-service-test
    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/node-api-service-test:${DOCKER_TAG}
    build:
      context: .
      dockerfile: docker/Dockerfile.api-test
    depends_on:
      - base
    user: ${USERID}:${GROUPID}
    networks:
      - chainpoint
    environment:
      NODE_ENV: ${NODE_ENV}
      NETWORK: ${NETWORK:-testnet}
      CHAINPOINT_CORE_BASE_URI: http://test.chainpoint.org
      LND_SOCKET: ${LND_SOCKET}
      VIRTUAL_HOST: api.local
      TENDERMINT_URI: http://abci:26657
      HOT_WALLET_ADDRESS: ${HOT_WALLET_ADDRESS}
      LND_MACAROON: ${LND_MACAROON}
      LND_TLS_CERT: ${LND_TLS_CERT}
      SESSION_SECRET: ${SESSION_SECRET}
    volumes:
      - ~/.chainpoint/core/.lnd:/root/.lnd
      - ~/.chainpoint/core/data/keys/ecdsa_key.pem:/run/secrets/ECDSA_PKPEM
      - ./node-api-service/lib/endpoints:/home/node/app/lib/endpoints/
      - ./node-api-service/server.js:/home/node/app/server.js
      - ./node-api-service/test:/home/node/app/test
    tty: true

  # Bitcoin Transmit
  # Send Calendar Block Merkle roots to be embedded in a BTC transaction.
  #
  #  btc-tx:
  #    restart: always
  #    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/node-btc-tx-service:${DOCKER_TAG}
  #    build:
  #      context: .
  #      dockerfile: docker/Dockerfile.btc-tx
  #    container_name: btc-tx-core
  #    user: ${USERID}:${GROUPID}
  #    depends_on:
  #      - base
  #      - rabbitmq
  #    networks:
  #      - chainpoint
  #    environment:
  #      NODE_ENV: ${NODE_ENV}
  #      NETWORK: ${NETWORK:-testnet}
  #      CHAINPOINT_CORE_BASE_URI: ${CHAINPOINT_CORE_BASE_URI}
  #      HOT_WALLET_ADDRESS: ${HOT_WALLET_ADDRESS}
  #      LND_SOCKET: ${LND_SOCKET}
  #    volumes:
  #      - ~/.chainpoint/core/.lnd:/root/.lnd
  #    tty: true

  # Lightning Invoice Monitor
  # Monitor and report on the state of LND invoices.
  #
  lnd-mon:
    restart: always
    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/node-lnd-mon-service:${DOCKER_TAG}
    build:
      context: .
      dockerfile: docker/Dockerfile.lnd-mon
    container_name: lnd-mon-core
    user: ${USERID}:${GROUPID}
    depends_on:
      - base
      - redis
      - lnd
    networks:
      - chainpoint
    environment:
      NETWORK: ${NETWORK:-testnet}
      NODE_ENV: ${NODE_ENV}
      HOT_WALLET_PASS: ${HOT_WALLET_PASS}
      LND_SOCKET: ${LND_SOCKET}
    volumes:
      - ~/.chainpoint/core/.lnd:/root/.lnd
    tty: true

  # ln-accounting
  # Returns accounting reports in harmony format for lnd node
  ln-accounting:
    image: tierion/ln-accounting
    ports:
      - '9000'
    environment:
      NETWORK: ${NETWORK}
      LND_DIR: /root/.lnd
      LND_SOCKET: ${LND_SOCKET}
      ACCOUNTING_PORT: ${ACCOUNTING_PORT:-9000}
    volumes:
      - ~/.chainpoint/core/.lnd:/root/.lnd:ro
    networks:
      - chainpoint

  # Proof State
  # Encapsulates all persistent data storage for partial proof data.
  #
  proof-state:
    restart: always
    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/node-proof-state-service:${DOCKER_TAG}
    build:
      context: .
      dockerfile: docker/Dockerfile.proof-state
    user: ${USERID}:${GROUPID}
    depends_on:
      - base
      - rabbitmq
      - postgres
    networks:
      - chainpoint
    environment:
      NODE_ENV: ${NODE_ENV}
      NETWORK: ${NETWORK:-testnet}
      CHAINPOINT_CORE_BASE_URI: ${CHAINPOINT_CORE_BASE_URI}
    tty: true

  # Proof Generation
  # Responsible for constructing, signing, and validating Chainpoint proofs
  # for Calendar, Bitcoin, and Ethereum attestation levels.
  #
  proof-gen:
    restart: always
    image: gcr.io/chainpoint-registry/github_chainpoint_chainpoint-core/node-proof-gen-service:${DOCKER_TAG}
    build:
      context: .
      dockerfile: docker/Dockerfile.proof-gen
    user: ${USERID}:${GROUPID}
    depends_on:
      - base
      - rabbitmq
      - postgres
    networks:
      - chainpoint
    environment:
      NODE_ENV: ${NODE_ENV}
      NETWORK: ${NETWORK:-testnet}
    tty: true

  # RabbitMQ
  #
  # Admin Page (username:pass)
  # http://127.0.0.1:15673/ (rabbitmq:rabbitmq)
  #
  # LOCAL PORTS:
  # amqp : 5673 (+1 over default)
  # http : 15673 (+1 over default)
  #
  rabbitmq:
    restart: always
    image: rabbitmq:3.6.11-management-alpine
    container_name: rabbitmq-core
    hostname: rabbitmq
    user: ${USERID}:${GROUPID}
    ports:
      - 5673:5672
      - 15673:15672
    environment:
      RABBITMQ_DEFAULT_USER: chainpoint
      RABBITMQ_DEFAULT_PASS: chainpoint
    networks:
      - chainpoint

  # Redis
  #
  redis:
    restart: always
    image: redis:4.0.9
    container_name: redis-core
    user: ${USERID}:${GROUPID}
    ports:
      - '6382:6379'
    volumes:
      - ./redis/redis.conf:/usr/local/etc/redis/redis.conf:ro
      - ~/.chainpoint/core/data/redis:/data:Z
    networks:
      - chainpoint
    command:
      - redis-server
      - /usr/local/etc/redis/redis.conf

  # Postgres
  #
  # See : https://hub.docker.com/_/postgres/
  # Note: Connect locally on OS X:
  #
  # Installs local client 'psql'
  #   brew install postgres
  #
  # Connect (uname/pass chainpoint/chainpoint):
  #   psql -h 127.0.0.1 -U chainpoint
  #
  postgres:
    restart: always
    image: postgres:11.2
    container_name: postgres-core
    user: ${USERID}:${GROUPID}
    volumes:
      - ~/.chainpoint/core/data/postgresql:/var/lib/postgresql/data:Z
    environment:
      POSTGRES_USER: chainpoint
      POSTGRES_PASSWORD: chainpoint
    ports:
      - '5433:5432'
    networks:
      - chainpoint
